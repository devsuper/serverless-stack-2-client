const dev = {
  s3: {
    REGION: "us-east-1",
    BUCKET: "notes-app-2-api-dev-attachmentsbucket-ukrw0291nmpf"
  },
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://z97c4b94zg.execute-api.us-east-1.amazonaws.com/dev"
  },
  cognito: {
    REGION: "us-east-1",
    USER_POOL_ID: "us-east-1_fNCp2mNvd",
    APP_CLIENT_ID: "7a3g29ltonc8nv7979fphmgdlu",
    IDENTITY_POOL_ID: "us-east-1:926e7cd4-9ee0-4eec-b315-c788fc53672e"
  },
  STRIPE_KEY: "pk_test_0KoQkp17slfS9HzKpO5SJdXV"
};

const prod = {
  s3: {
    REGION: "us-east-1",
    BUCKET: "notes-app-2-api-prod-attachmentsbucket-al7qikjsv2k7"
  },
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://qu7ge9eqwl.execute-api.us-east-1.amazonaws.com/prod"
  },
  cognito: {
    REGION: "us-east-1",
    USER_POOL_ID: "us-east-1_FxuAElJw0",
    APP_CLIENT_ID: "404h0090cacv945q14209kat37",
    IDENTITY_POOL_ID: "us-east-1:ab8d55f9-cd12-4d92-93a6-eda30a11b21b"
  },
  STRIPE_KEY: "sk_test_XPaWZHylDIinNCkHnDNP3t4K"
};

// Default to dev if not set
const config = process.env.REACT_APP_STAGE === "prod" ? prod : dev;

export default {
  // Add common config values here
  MAX_ATTACHMENT_SIZE: 5000000,
  ...config
};
